// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: "AIzaSyD5Kgmpx2WBA2gfPAfKHbHlo9NpF_biO9w",
  authDomain: "ionic-project-e76ad.firebaseapp.com",
  projectId: "ionic-project-e76ad",
  storageBucket: "ionic-project-e76ad.appspot.com",
  messagingSenderId: "242378905953",
  appId: "1:242378905953:web:842eed4d242e950b95cd20",
  measurementId: "G-LRBWPQWJDR",
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
