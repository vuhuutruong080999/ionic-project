import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FoodsPage } from './foods/foods.page';
import { SearchPage } from './foods/search/search.page';
import { DetailPage } from './foods/detail/detail.page';
import { FavoritePage } from './foods/favorite/favorite.page';
import { ListfoodComponent } from './foods/listfood/listfood.component';
import { ErrorpageComponent } from './errorpage/errorpage.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { LoginComponent } from './login/login.component';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { SignUpComponent } from './sign-up/sign-up.component';
import { UserInfoComponent } from './foods/search/user-info/user-info.component';

@NgModule({
  declarations: [
    AppComponent,
    FoodsPage,
    SearchPage,
    DetailPage,
    FavoritePage,
    ListfoodComponent,
    ErrorpageComponent,
    LoginComponent,
    SignUpComponent,
    UserInfoComponent,
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
      mode: 'ios',
    }),
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment),
    AngularFireAuthModule,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
