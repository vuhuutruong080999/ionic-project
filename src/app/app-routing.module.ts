import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { ErrorpageComponent } from './errorpage/errorpage.component';
import { DetailPage } from './foods/detail/detail.page';
import { FavoritePage } from './foods/favorite/favorite.page';
import { FoodsPage } from './foods/foods.page';
import { ListfoodComponent } from './foods/listfood/listfood.component';
import { SearchPage } from './foods/search/search.page';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { AuthGuard } from './shared/guard/auth.guard';
import { UserInfoComponent } from './foods/search/user-info/user-info.component';

const routes: Routes = [
  {
    path: 'foods',
    component: FoodsPage,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'search',
        children: [
          {
            path: '',
            component: SearchPage,
          },
          {
            path: ':categoryId/food',
            children: [
              { path: '', component: ListfoodComponent },
              { path: ':foodId', component: DetailPage },
            ],
          },
        ],
      },
      {
        path: 'favorite',
        children: [
          {
            path: '',
            component: FavoritePage,
          },
          {
            path: 'category/:categoryId/food/:foodId',
            component: DetailPage,
          },
        ],
      },
      { path: 'info', component: UserInfoComponent },
      { path: '', redirectTo: 'search', pathMatch: 'full' },
    ],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  { path: 'register-user', component: SignUpComponent },
  {
    path: '',
    redirectTo: 'foods',
    pathMatch: 'full',
  },
  { path: '**', component: ErrorpageComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
