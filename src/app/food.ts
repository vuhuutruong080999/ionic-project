export interface IFood {
  id: string;
  categoryId: string;
  name: string;
  imageURL: string;
  description: string;
  price: number;
}
export interface ICategory {
  id: string;
  name: string;
  imageURL: string;
}
