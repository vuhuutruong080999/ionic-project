import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICategory, IFood } from './food';

@Injectable({
  providedIn: 'root',
})
export class FoodsService {
  private _favoriteList: IFood[] =
    JSON.parse(localStorage.getItem('favoriteList')) ?? [];
  private _url = 'https://601369be54044a00172dd4b7.mockapi.io/category';

  constructor(private _http: HttpClient) { }

  getCategories(): Observable<ICategory[]> {
    return this._http.get<ICategory[]>(this._url);
  }

  getFoodsByCategoryId(categoryId: string): Observable<IFood[]> {
    return this._http.get<IFood[]>(this._url + '/' + categoryId + '/food');
  }

  getFoodById(categoryId: string, id: string): Observable<IFood> {
    return this._http.get<IFood>(this._url + '/' + categoryId + '/food/' + id);
  }

  getFavoriteFoods(): Array<IFood> {
    return this._favoriteList;
  }

  setFavoriteFoods(food: IFood): void {
    let count = 0;
    this._favoriteList.forEach((foodF) => {
      if (foodF.id === food.id) {
        count++;
      }
    });
    if (count === 0) { this._favoriteList.push(food); }
    else {
      const favoriteFood = this._favoriteList.filter(
        (foodL) => foodL.id !== food.id
      );
      this._favoriteList = favoriteFood;
    }
    localStorage.setItem('favoriteList', JSON.stringify(this._favoriteList));
  }
}
