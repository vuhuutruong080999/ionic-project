import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IFood } from 'src/app/food';
import { FoodsService } from 'src/app/foods.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  food: IFood;
  categoryId: string;
  foodId: string;
  check = false;
  constructor(
    private _router: ActivatedRoute,
    private _foodsService: FoodsService
  ) { }

  ngOnInit() {
    this._router.paramMap.subscribe((paramMap) => {
      this.categoryId = paramMap.get('categoryId');

      this.foodId = paramMap.get('foodId');
      console.log(this.categoryId, this.foodId);
    });
    this._foodsService
      .getFoodById(this.categoryId, this.foodId)
      .subscribe((food) => (this.food = food));
    this.check = this.checkFavorite(this.foodId);
  }
  addFavorite(food: IFood): void {
    this._foodsService.setFavoriteFoods(food);
    this.check = this.checkFavorite(this.foodId);
  }
  checkFavorite(foodId): boolean {
    let check = this._foodsService
      .getFavoriteFoods()
      .find((food) => food.id === foodId);
    if (check) {
      return true;
    }
    return false;
  }
}
