import { Component, OnInit } from '@angular/core';
import { IFood } from 'src/app/food';
import { FoodsService } from 'src/app/foods.service';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.page.html',
  styleUrls: ['./favorite.page.scss'],
})
export class FavoritePage implements OnInit {
  favoriteList: Array<IFood>;
  constructor(private _foodsService: FoodsService) { }

  ngOnInit() { }
  ionViewWillEnter() {
    this.favoriteList = this._foodsService.getFavoriteFoods();
  }
}
