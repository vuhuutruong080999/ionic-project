import { Component, OnInit } from '@angular/core';
import { ICategory, IFood } from 'src/app/food';
import { FoodsService } from 'src/app/foods.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  categories: Array<ICategory>;
  hotdogs: Array<IFood>;
  drinks: Array<IFood>;
  snacks: Array<IFood>;
  constructor(private _foodsService: FoodsService) { }

  ngOnInit() {
    this._foodsService
      .getCategories()
      .subscribe((data) => (this.categories = data));
    this._foodsService
      .getFoodsByCategoryId('7')
      .subscribe((data) => (this.hotdogs = data));
    this._foodsService
      .getFoodsByCategoryId('8')
      .subscribe((data) => (this.drinks = data));
    this._foodsService
      .getFoodsByCategoryId('5')
      .subscribe((data) => (this.snacks = data));
  }
}
