import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ICategory, IFood } from 'src/app/food';
import { FoodsService } from 'src/app/foods.service';

@Component({
  selector: 'app-listfood',
  templateUrl: './listfood.component.html',
  styleUrls: ['./listfood.component.css'],
})
export class ListfoodComponent implements OnInit {
  foods: Array<IFood>;
  categoryName: string;
  constructor(
    private _route: ActivatedRoute,
    private _foodsService: FoodsService
  ) { }

  ngOnInit(): void {
    this._route.paramMap.subscribe((params) => {
      this._foodsService.getCategories().subscribe((categories) => {
        const category: ICategory[] = categories.filter(
          (category) => category.id === params.get('categoryId')
        );
        this.categoryName = category[0].name;
      });
      this._foodsService
        .getFoodsByCategoryId(params.get('categoryId'))
        .subscribe((foods) => (this.foods = foods));
    });
  }
}
